import { Component } from '@angular/core';
import { setTimeout } from 'timers-browserify';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lab5';
  sensorId = 100;
  sensorStatus = "выключен";
  sensorCreationStatus = "Ничего не было добавлено";
  sensorSn = "000-000-000";
  allowNewSensors = false;

  constructor() {
    setTimeout(() => {
      this.allowNewSensors = true;
    }, 2000);
  }

  onCreateSensor() {
    this.sensorCreationStatus = "Датчик был добавлен";
    this.sensorCreationStatus = "Был добавлен датчик с номером " + this.sensorSn;
  }
}
